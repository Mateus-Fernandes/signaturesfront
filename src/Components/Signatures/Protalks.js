import React, {useEffect, useState} from 'react';
import MaterialTable from "material-table";
import { Redirect } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditAttributesIcon from '@material-ui/icons/EditAttributes';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import {useDropzone} from 'react-dropzone';
import Grid from '@material-ui/core/Grid';
import api from "../../services/api";
import Swal from 'sweetalert2'

const thumbsContainer = {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 16
  };
  
  const thumb = {
    display: 'inline-flex',
    borderRadius: 2,
    border: '1px solid #eaeaea',
    marginBottom: 8,
    marginRight: 8,
    marginTop: 20,
    width: 700,
    height: 70,
    padding: 4,
    boxSizing: 'border-box'
  };
  
  const thumbInner = {
    display: 'flex',
    minWidth: 0,
    overflow: 'hidden'
  };
  
  const img = {
    display: 'block',
    width: 'auto',
    height: '100%'
  };

const useStyles = makeStyles(theme => ({
    fab: {
      margin: theme.spacing(1),
      backgroundColor: '#41ad15'
    },
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
}));

export default function Signatures() {
    const classes = useStyles();

    const [create, setCreate] = React.useState(false);
    const [files, setFiles] = useState([]);
    const {getRootProps, getInputProps} = useDropzone({
        accept: 'image/*',
        onDrop: acceptedFiles => {
          setFiles(acceptedFiles.map(file => Object.assign(file, {
            preview: URL.createObjectURL(file)
          })));
          
        }
      });
      
      const thumbs = files.map(file => (
        <div style={thumb} key={file.name}>
          <div style={thumbInner}>
            <img
              src={file.preview}
              style={img}
            />
          </div>
        </div>
      ));

      useEffect(() => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach(file => URL.revokeObjectURL(file.preview));
      
      }, [files]);    
      
      const saveProtalks = () =>{
       
        var bodyFormData = new FormData();
        bodyFormData.append('image', files[0]); 
        
        
        api.post('/protalks', bodyFormData, { headers: {'Content-Type': 'multipart/form-data' }})
            .then(function (response) {
                Swal.fire({
                    title: 'Sucesso!',
                    text: 'Imagem atualizada com sucesso!',
                    icon: 'success',
                    confirmButtonText: 'Cool'
                  })
                
            })
            .catch(function (response) {
                //handle error
                console.log(response);
            });        
      }

    return (
        <div>

        <div className="breadCrumb">
            <h2>Protalks</h2>
            <h4>Home > Protalks</h4>
        </div>
        <div className="main"> 
            <div className="primaryBox">
            <Grid item xs={12}>
                    <section className="container">
                        <h3 style={{fontSize: 24, marginBottom: 10}}>Alteração da imagem do Protalks</h3>
                        <div {...getRootProps({className: 'dropzone'})}>
                            <input {...getInputProps()} />
                            <p style={{marginBottom: 10}}>Largue o arquivo aqui ou clique para selecionar...</p>
                            <Button
                                variant="contained"
                                color="secondary"
                                size="medium"
                                className="botaoAssinatura"
                                startIcon={<SaveIcon />}
                            >
                               Selecionar imagem
                            </Button>                             
                        </div>
                        <aside>
                            {thumbs}
                        </aside>
                    </section>

                    </Grid>    
                    <Button
                    variant="contained"
                    color="secondary"
                    size="medium"
                    style={{marginTop: 20}}
                    className="botaoAssinatura"
                    onClick={saveProtalks}
                    startIcon={<SaveIcon />}
                >
                    Salvar imagem Protalks
                </Button> 
            </div>
            
        </div>
        </div>
    )

}