import React, {useState, useEffect, useCallback, useRef} from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import Paper from '@material-ui/core/Paper';
import {useDropzone} from 'react-dropzone';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import FilledInput from '@material-ui/core/FilledInput';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slider from '@material-ui/core/Slider';
import Cropper from 'react-easy-crop'
import getCroppedImg from './cropImg'
import api from "../../services/api";
import VisibilityIcon from '@material-ui/icons/Visibility';
const axios = require('axios');


const thumbsContainer = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginTop: 16
};

const thumb = {
  display: 'inline-flex',
  borderRadius: 2,
  border: '1px solid #eaeaea',
  marginBottom: 8,
  marginRight: 8,
  width: 100,
  height: 100,
  padding: 4,
  boxSizing: 'border-box'
};

const thumbInner = {
  display: 'flex',
  minWidth: 0,
  overflow: 'hidden'
};

const img = {
  display: 'block',
  width: 'auto',
  height: '100%'
};

const useStyles = makeStyles(theme => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: '100%'
    },
    dense: {
      marginTop: theme.spacing(2),
    },
    menu: {
      width: 200,
    },
    buttonFile: {
        margin: theme.spacing(1),
        width: '100%',
        marginTop: 16,
        paddingTop: 15,
        paddingBottom: 15
    },
      withoutLabel: {
        marginTop: theme.spacing(3),
      },
      margin: {
          marginTop: theme.spacing(1)
      }
  
}));

function SignatureGenerate (props){



    console.log(props);
    return(
        <>
            <div className="signature" ref={props.refContainer}>

            <div className="signature-content">
                <div className="photo">
                    <img src={'https://api.diwe.com.br/' + props.values.image.replace("public", "storage")} width="70" height="70" alt=""/>
                </div>

                <div className="signature-info">
                    <h2>{props.values.name}</h2>
                    <h3>{props.values.job}</h3>
                    <p><a href={props.values.email}>{props.values.email}</a></p>
                </div>

                <div className="social">
                {props.values.linkedin ? (
                    <a href={props.values.linkedin} target="_blank" title="LinkedIn">
                        <img src="https://diwe.com.br/diwe-signature/img/social/linkedin.png" width="28" height="28" alt="LinkedIn" />
                    </a>
                    ): (
                        <>
                        </>
                    )
                    }

                    {props.values.facebook ? (
                    <a href={props.values.facebook} target="_blank" title="Facebook">
                        <img src="https://diwe.com.br/diwe-signature/img/social/facebook.png" width="28" height="28" alt="Facebook"/>
                    </a>
                    ): (
                        <>
                        </>
                    )
                    }
                    {props.values.whats ? (
                    <a href={props.values.whats} target="_blank" title="WhatsApp">
                        <img src="https://diwe.com.br/diwe-signature/img/social/whatsapp.png" width="28" height="28" alt="WhatsApp"/>
                    </a>
                    ): (
                        <>
                        </>
                    )
                    }
                    {props.values.instagram ? (
                    <a href={props.values.instagram} target="_blank" title="Instagram">
                        <img src="https://diwe.com.br/diwe-signature/img/social/instagram.png" width="28" height="28" alt="Instagram"/>
                    </a>
                    ): (
                        <>
                        </>
                    )
                    }     
                </div>
            </div>

            <div className="contact-info">
                <div className="phone">
                    <img src="https://diwe.com.br/diwe-signature/img/phone.png" width="18" height="18" alt=""/>
                    <p>11 4614-6805 | 47 3025-4320</p>
                </div>
                
                <a href="https://diwe.com.br" title="diwe.com.br">diwe.com.br</a>
            </div>

            <a href="http://bit.ly/31IqLO4" target="_blank" title="ProTalks">
                <img src="https://api.diwe.com.br/storage/protalks/protalks.gif" width="431" height="76" alt="ProTalks"/>
            </a>
            </div>
        </>
    );
}

export default function SignaturesEdit(props) {
    const classes = useStyles();

    const [files, setFiles] = useState([]);
    const [imgCrop, setImgCrop] = useState([]);
    const {getRootProps, getInputProps} = useDropzone({
      accept: 'image/*',
      onDrop: acceptedFiles => {
        setFiles(acceptedFiles.map(file => Object.assign(file, {
          preview: URL.createObjectURL(file)
        })));
        setImgCrop(acceptedFiles.find(element => element.preview !== null));
        handleClickOpenPicture();
      }
    });
    
    const thumbs = files.map(file => (
      <div style={thumb} key={file.name}>
        <div style={thumbInner}>
          <img
            src={file.preview}
            style={img}
          />
        </div>
      </div>
    ));
  
    useEffect(() => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach(file => URL.revokeObjectURL(file.preview));
    }, [files]);
    
    useEffect(() => {
        console.log(props)
        axios.get('https://api.diwe.com.br/api/users/' + props.match.params.id)
        .then(function(response){
                setValues({...values,
                    name: response.data.data.name,
                    email: response.data.data.email,
                    job: response.data.data.job,
                    linkedin: response.data.data.linkedin,
                    facebook: response.data.data.facebook,
                    instagram: response.data.data.instagram,
                    whats: response.data.data.whats,
                    image: response.data.data.image
                });    
                
                setCroppedImage('https://api.diwe.com.br/' + response.data.data.image.replace("public", "storage"))
        });  
      }, []);

    const [values, setValues] = React.useState({
        name: '',
        email: '',
        job: '',
        password: '',
        linkedin: '',
        facebook: '',
        instagram : '',
        image: '',
        whats : '',
        showPassword: false,
    });

    const [signatureHTML, setSignatureHTML] = React.useState('Opa');


    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
      };    

    const handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
    };

    const handleMouseDownPassword = event => {
        event.preventDefault();
    };    

    const [open, setOpen] = React.useState(false);
    const [openPicture, setOpenPicture] = React.useState(false);

    const refContainer = useRef(null);


    const handleView = () => {
        setOpen(true);
    }

    const handleClickOpen = () => {
        urltoFile(croppedImage, 'teste.png').then(function(fileTemp){
            var bodyFormData = new FormData();
            bodyFormData.set('name', values.name);
            bodyFormData.set('email', values.email);
            bodyFormData.set('job', values.job);
            bodyFormData.set('password', values.password);
            bodyFormData.set('linkedin', values.linkedin);
            bodyFormData.set('facebook', values.facebook);
            bodyFormData.set('instagram', values.instagram);
            bodyFormData.set('whats', values.whats);
            bodyFormData.set('_method', 'put');
            if(croppedImage){
                bodyFormData.append('image', fileTemp);
            }
            console.log('vou exibir o que eu tenho');
            console.log(bodyFormData);
            
            axios.post('https://api.diwe.com.br/api/users/'+props.match.params.id, bodyFormData, { headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    //handle success
                    console.log(response);
                    setValues({...values,
                        name: response.data.data.name,
                        email: response.data.data.email,
                        job: response.data.data.job,
                        linkedin: response.data.data.linkedin,
                        facebook: response.data.data.facebook,
                        instagram: response.data.data.instagram,
                        whats: response.data.data.whats,
                        image: response.data.data.image
                    });
                    setOpen(true);
                })
                .catch(function (response) {
                    //handle error
                    console.log(response);
                });
        })
 
            

      
    };
  
    const handleClickOpenPicture = () => {
        setOpenPicture(true);
    }
  
    const handleClose = () => {
      setOpen(false);
    };    

    const handleClosePicture = () => {
        setOpenPicture(false);
    }

    function urltoFile(url, filename, mimeType){
        mimeType = mimeType || (url.match(/^data:([^;]+);/)||'')[1];
        return (fetch(url)
            .then(function(res){return res.arrayBuffer();})
            .then(function(buf){return new File([buf], filename, {type:mimeType});})
        );
    }
    

    const [crop, setCrop] = useState({ x: 0, y: 0 })
    const [zoom, setZoom] = useState(1);
    const [croppedAreaPixels, setCroppedAreaPixels] = useState(null)
    const [croppedImage, setCroppedImage] = useState(null)
    const [rotation, setRotation] = useState(0)
    const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
        setCroppedAreaPixels(croppedAreaPixels) 
    }, []);
  
    const showCroppedImage = useCallback(async () => {
        console.log(files);
        try {
          const croppedImage = await getCroppedImg(
            files[0].preview,
            croppedAreaPixels,
            rotation
          )
          console.log('donee', { croppedImage })
          setCroppedImage(croppedImage)
          setOpenPicture(false);
        } catch (e) {
            console.log('deu erro aqui');
          console.error(e)
        }
      }, [croppedAreaPixels, rotation])


    return (
        <div>
        <div className="breadCrumb">
            <h2>Assinaturas</h2>
            <h4>Home > Assinaturas > Atualizar Assinatura</h4>
        </div>
        <div className="main"> 
            <div className="primaryBox">
                <form className={classes.container} noValidate autoComplete="off">
                
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <TextField
                            id="outlined-name"
                            label="Nome"
                            className={classes.textField}
                            value={values.name}
                            onChange={handleChange('name')}
                            margin="normal"
                            variant="outlined"
                        />
                        <TextField
                            id="outlined-name"
                            label="Email"
                            className={classes.textField}
                            value={values.email}
                            onChange={handleChange('email')}
                            margin="normal"
                            variant="outlined"
                        />
                        <TextField
                            id="outlined-name"
                            label="Cargo"
                            className={classes.textField}
                            value={values.job}
                            onChange={handleChange('job')}
                            margin="normal"
                            variant="outlined"
                        />
                        <TextField
                            id="outlined-name"
                            label="Linkedin"
                            className={classes.textField}
                            value={values.linkedin}
                            onChange={handleChange('linkedin')}
                            margin="normal"
                            variant="outlined"
                        />
                        <TextField
                            id="outlined-name"
                            label="Facebook"
                            className={classes.textField}
                            value={values.facebook}
                            onChange={handleChange('facebook')}
                            margin="normal"
                            variant="outlined"
                        />
                        <TextField
                            id="outlined-name"
                            label="Instagram"
                            className={classes.textField}
                            value={values.instagram}
                            onChange={handleChange('instagram')}
                            margin="normal"
                            variant="outlined"
                        />
                        <TextField
                            id="outlined-name"
                            label="Whats"
                            className={classes.textField}
                            value={values.whats}
                            onChange={handleChange('whats')}
                            margin="normal"
                            variant="outlined"
                        />    
                    </Grid>  

                    <Grid item xs={12}>
                    <section className="container">
                        <div {...getRootProps({className: 'dropzone'})}>
                            <input {...getInputProps()} />
                            <p style={{marginBottom: 10}}>Largue o arquivo aqui ou clique para selecionar...</p>
                            <Button
                                variant="contained"
                                color="secondary"
                                size="medium"
                                className="botaoAssinatura"
                                startIcon={<SaveIcon />}
                            >
                               Alterar foto da assinatura
                            </Button>                            
                        </div>
                        <aside style={thumbsContainer}>
                            <img style={{width: 150, borderRadius: '100%'}} src={croppedImage} />
                        </aside>
                    </section>
                    </Grid>                                                                                                                 
                    <Grid item xs={12}>
                    <Button
                        variant="contained"
                        color="secondary"
                        size="medium"
                        onClick={handleClickOpen}
                        className="botaoAssinatura"
                        startIcon={<SaveIcon />}
                    >
                        Atualizar Assinatura
                    </Button>
                    <Button
                        variant="contained"
                        color="secondary"
                        size="medium"
                        style={{marginLeft: 20, background: '#292929'}}
                        onClick={handleView}
                        className="botaoAssinatura"
                        startIcon={<VisibilityIcon />}
                    >
                        Visualizar Assinatura
                    </Button>                    
                    
                    </Grid>        
                </Grid>   
                <Dialog
                    open={openPicture}
                    onClose={handleClosePicture}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"Ajuste sua imagem"}</DialogTitle>
                    <DialogContent>
                    <DialogContentText id="alert-dialog-description">

                    <div className="crop-container">
                        <Cropper
                        image={imgCrop.preview}
                        crop={crop}
                        zoom={zoom}
                        aspect={1}
                        onCropChange={setCrop}
                        cropShape="round"
                        showGrid={false}
                        onCropComplete={onCropComplete}
                        onZoomChange={setZoom}
                        />
                    </div>
                    <div className="controls">
                        <Slider
                        value={zoom}
                        min={1}
                        max={3}
                        step={0.1}
                        aria-labelledby="Zoom"
                        onChange={(e, zoom) => setZoom(zoom)}
                        classes={{ container: 'slider' }}
                        />
                    </div>

                    </DialogContentText>
                    </DialogContent>
                    <DialogActions>

                    <Button onClick={showCroppedImage} color="secondary" autoFocus>
                        Mostrar resultado
                    </Button>
                    </DialogActions>
                </Dialog>                     
                <Dialog
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"Selecione o conteúdo do popup e aperte CTRL+C"}</DialogTitle>
                    <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                    <SignatureGenerate refContainer={refContainer} values={values}></SignatureGenerate>
                    </DialogContentText>
                    </DialogContent>
                    <DialogActions>

                    <Button onClick={handleClose} className="botaoAssinatura" autoFocus>
                        Fechar
                    </Button>
                    </DialogActions>
                </Dialog>
                </form>
            </div>
        </div>
        </div>
    )

}