import React from 'react';
import MaterialTable from "material-table";
import { Redirect } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditAttributesIcon from '@material-ui/icons/EditAttributes';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';


const useStyles = makeStyles(theme => ({
    fab: {
      margin: theme.spacing(1),
      backgroundColor: '#41ad15'
    },
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
}));

export default function Signatures() {
    const classes = useStyles();

    const [create, setCreate] = React.useState(false);

    const [redirect, setRedirect] = React.useState({
        path: '',
        status: false,
    })
    const renderRedirect = () => {
        if (create) {
          return <Redirect to="/signatures/create" />
        }
    }

    return (
        <div>
        {renderRedirect()}

        {redirect.status ? (
            <Redirect to={'/signatures/edit/'+redirect.path} />
        ) : (
            <></>
        )
        
    }

        <div className="breadCrumb">
            <h2>Assinaturas</h2>
            <h4>Home > Assinaturas</h4>
        </div>
        <div className="main"> 
            
            <MaterialTable
            options={{
                search: false,
                actionsColumnIndex: -1,
                paging: false
            }
            }
            columns={[
            {
                    title: 'Avatar',
                    field: 'avatar',
                    render: rowData => (
                        <img
                        style={{ height: 36, borderRadius: '50%' }}
                        src={'https://api.diwe.com.br/' + rowData.image.replace("public", "storage")}
                        />
                    ),
                },                
                { title: "Nome", field: "name" },
                { title: "Email", field: "email" },
            ]}
            data={query =>
                new Promise((resolve, reject) => {
                  let url = 'https://api.diwe.com.br/api/users'
                  fetch(url)
                    .then(response => response.json())
                    .then(result => {
                      resolve({
                        data: result.data,
                      })
                    })
                })
              }
            actions={[
                {
                  icon: () => {
                    return(
                        <Button color="inherit" className="addbtn">
                            Adicionar <AddIcon />
                        </Button>
                    )
                  },
                  tooltip: 'Adicionar Assiantura',
                  isFreeAction: true,
                  onClick: (event) => { setCreate(true) }
                },
                {
                    icon: () => {
                        return(
                        <Button
                            variant="contained"
                            color="primary"
                            size="small"
                            className={classes.button}
                            startIcon={<SaveIcon />}
                          >
                                Editar
                            </Button>
                        )
                      },
                    tooltip: 'Editar',
                    onClick: (event, rowData) => {
                    
                      setRedirect({
                          path: rowData.id,
                          status: true
                      });
                      
                    }
                }                
            ]}
            title="Assinaturas cadastradas"
            />

        </div>
        </div>
    )

}