import axios from "axios";
import { getToken } from "./auth";
const USER_TOKEN = localStorage.getItem('token');
//console.log(USER_TOKEN)

const api = axios.create({
  baseURL: "https://api.diwe.com.br/api",
});


api.interceptors.request.use(async config => {
  const token = getToken();
  
  if (token) {
    config.headers.Authorization = `${token}`;
  }
  //console.log(config);
  return config;
});

api.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  if (401 === error.response.status) {
    localStorage.removeItem("@Diwe-Token"); 
    //window.location = '/';
  }
});

export default api;