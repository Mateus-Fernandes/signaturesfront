import React from 'react';
import logo from './logo.svg';
import './App.css';
import Routes from './routes'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import NavContextProvider from './Components/appbar/NavContext';

import purple from '@material-ui/core/colors/purple';
import green from '@material-ui/core/colors/green';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#212529'
    },
    secondary: {
      main: '#faa702'
    },
  },
  status: {
    danger: 'orange',
  },
  background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <NavContextProvider>
        <Routes></Routes>
      </NavContextProvider>
    </ThemeProvider>
    
  );
}

export default App;
