import React, {useContext} from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import AppBar from "./Components/appbar/Appbar";
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Signatures from './Components/Signatures/Signatures';
import Protalks from './Components/Signatures/Protalks';
import SignaturesCreate from './Components/Signatures/SignaturesCreate';
import SignaturesEdit from './Components/Signatures/SignaturesEdit';
import SignIn from "./pages/SignIn";
import { isAuthenticated } from "./services/auth";
import { NavContext } from './Components/appbar/NavContext';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated() ? (
        <React.Fragment>
        <Component {...props} />
        </React.Fragment>
      ) : (
        <React.Fragment>
        <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        </React.Fragment>
      )
    }
  />
);

const useStyles = makeStyles(theme => ({
    root: {
      display: 'flex',
    },
    appBarSpacer: {
        width: 240,
        minWidth: 240,
    },
    content: {
      flexGrow: 1,
      height: '100vh',
      display: 'flex',
      width: '100%',
      top: '64px',
      position: 'absolute',
      transition: 'all .5s',

    },
    container: {
      paddingTop: theme.spacing(4),
      paddingBottom: theme.spacing(4),
    },
  }));

export default function Routes() {
    const classes = useStyles();

    const { open } = useContext(NavContext);

    return (
    <BrowserRouter>
        <Switch>
        <Route exact path="/" component={SignIn} />
        <React.Fragment>
            <div className="root">
                <AppBar></AppBar>
                <main className={classes.content + ' ' + (open ? 'open_main' : 'close_main')}>
                    <div className={classes.appBarSpacer + ' spacerNav'} />
                    <Container maxWidth="lg" className={classes.container}>
                        <Route exact path="/signatures" component={Signatures} />
                        <Route exact path="/signatures/create" component={SignaturesCreate} />
                        <Route exact path="/signatures/edit/:id" component={SignaturesEdit} />
                        <Route exact path="/protalks" component={Protalks} />
                    </Container>
                </main>
            </div>
        </React.Fragment>
        </Switch>
    </BrowserRouter>
    )
};
