import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import api from "../../services/api";
import { login, logout, isAuthenticated } from "../../services/auth";
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import { Form, Container } from "./styles";
import Button from '@material-ui/core/Button';
import Logo from '../../logoblack.svg';

class SignIn extends Component {

  state = {
    email: "",
    password: "",
    error: ""
  };
  componentDidUpdate(prevProps) {
    if(isAuthenticated()) {
      this.props.history.push("/signatures");
    }else {
      window.localStorage.clear();
      localStorage.removeItem("@Diwe-Token"); 
    }
  }

  handleSignIn = async e => {
    e.preventDefault();
    const { email, password } = this.state;
    if (!email || !password) {
      this.setState({ error: "Preencha e-mail e senha para continuar!" });
    } else {
      try {
        const response = await api.post("/auth/login", { email, password });
        //console.log(response.headers.authorization);
        localStorage.clear();
        console.log(response);
        login(response.data.access_token);
        localStorage.setItem('user', response.data.token_type +' '+ response.data.access_token);
        this.props.history.push("/signatures");
      } catch (err) {
        this.setState({
          error:
            "Houve um problema com o login, verifique suas credenciais."
        });
      }
    }
  };

  render() {
    return (
      <Container className="login_bg">
        <Form onSubmit={this.handleSignIn} className="form_login">
          <h1><img src={Logo} /></h1>
          {this.state.error && <p>{this.state.error}</p>}
          <TextField
          label="Endereço de e-mail"
          onChange={e => this.setState({ email: e.target.value })}
          margin="normal"
          type="email"
          variant="outlined"
        />
        <TextField
          label="Senha"
          onChange={e => this.setState({ password: e.target.value })}
          margin="normal"
          type="password"
          variant="outlined"
        />
        <Button variant="contained" className="btn_login gradient" color="primary" size="large" type="submit">
          Entrar
        </Button>
        <div className="whiteLabel">
          Assinaturas App - Powered by Diwe Tech
        </div>
        </Form>
      </Container>
    );
  }
}

export default withRouter(SignIn);